from flask import Flask, request, render_template
import numpy as np
import pandas as pd
from sklearn import preprocessing
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler

print("Версия TensorFlow:", tf.__version__)

df1 = pd.read_csv("K:/Документы Инна/Обучение/DS/5 DS ВКР/X_nup1.csv")
df2 = pd.read_csv("K:/Документы Инна/Обучение/DS/5 DS ВКР/X_bp2.csv")
df_result = pd.concat([df2, df1], axis=1, join="inner")
del df_result['Unnamed: 0']
X0 = df_result[['Плотность. кг/м3',
                'модуль упругости. ГПа', 'Количество отвердителя. м.%',
                'Содержание эпоксидных групп.%_2', 'Температура вспышки. С_2',
                'Поверхностная плотность. г/м2', 'Потребление смолы. г/м2',
                'Угол нашивки. град', 'Шаг нашивки', 'Плотность нашивки']].values
y0 = df_result['Модуль упругости при растяжении. ГПа'].values
min_max_scaler_X0: MinMaxScaler = preprocessing.MinMaxScaler()
X0 = min_max_scaler_X0.fit_transform(X0)
min_max_scaler_y0 = preprocessing.MinMaxScaler()
y0 = min_max_scaler_y0.fit_transform(y0.reshape(-1, 1))
y0 = y0.ravel()

app = Flask(__name__)


def get_prediction(X0):
    model = tf.keras.models.load_model(r".\mlp_adam_3_2_250")
    y_pred = model.predict(X0)
    y_pred = min_max_scaler_y0.inverse_transform(y_pred)
    return f"Модуль упругости при растяжении. ГПа  {[[y_pred]]}"


@app.route('/')
def index():
    return "main"


@app.route('/predict/', methods=['post', 'get'])
def processing():
    message = ''
    if request.method == 'POST':
        X0 = request.form.get('username')

        X0_parameters = X0.split(" ")
        X0 = [float(param) for param in X0_parameters]
        X0 = np.array([X0])
        X0 = min_max_scaler_X0.fit_transform(X0)

        message = get_prediction(X0)

    return render_template('login.html', message=message)


if __name__ == '__main__':
   app.run()
